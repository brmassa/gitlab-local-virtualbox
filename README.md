# Personal/Local Gitlab+Gitlab Runner inside Virtualbox

Package needed to locally host a Gitlab instance to test specially gitlab CI/CD pipelines.

It's too slow and frustrating to rely on uploading a .gitlab.yml several times to a production Gitlab in order to check if the pipeline is ok.

## Pre install: Virtualbox

Create a Linux instance using Virtualbox with:
- image: any Linux distro, like Ubuntu
- disk at least 20gb (40gb sugested).
- for ports (Settings > Network > Advanced > Port Forwarding)
  - For the main site:
    - Host IP: **127.0.0.1**
    - Host Port: **9000** (any port that would like to type in the browser)
    - Guest Port: **80**
  - SSH:
    - Host IP: **127.0.0.1**
    - Host Port: **2222** (any port that would like to type in the terminal/putty/vscode)
    - Guest Port: **22**

## Install

* Clone this repository.
* ```cd <FOLDER>```
* ```sudo docker-compose -d```

## Config Gitlab

* In your browser, type localhost:9000
* user: admin, password: adminadmin
* explore!

## Config Gitlab Runner

* In the broswer, goto http://localhost:9000/admin/runners and press the "Register an instance runner" copy the value 
* Using SSH inside the virtual machine, then type
```bash
sudo docker exec -it gitlab_gitlab-runner_1 gitlab-runner register
```
* Follow the steps. use the code copied before 
 The runner will work globally for all the projects.

## Bugs

since the local instance might run out of space quite quickly, it might be useful to, time to time, SSH into the instance and delete unused images.
```bash
sudo docker image prune -a
```
